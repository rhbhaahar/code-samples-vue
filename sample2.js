import store from '@/store';
import axios from 'axios';
import config from '@/../configs';

const apiUrl = config ? config.api : null;

axios.interceptors.response.use((response) => {

  const setError = response.data && response.data.statusCode
    ? !(response.data.statusCode < 400 || response.config.errorsPass)
    : true
  if (setError && !response.config.passAllErrors) {
    store.commit('setApiError', {
      errorMsg: response.data,
      url: response.config.url,
      method: response.config.method,
    });
  }

  return response;

}, (error) => { Promise.reject(error); });

let reauthRunned = false;

export default {
  actions: {
    async apiGetCollections(context, data) {
      if (!data.eventId) return false;
      const id = data.standId || data.eventId;
      const prefix = data.standId ? 'stand' : 'event';

      let url = '';
      let apiType = '';
      if (data.user) {
        url = `https://${apiUrl}/${prefix}/${id}/collections`;
        apiType = 'getApi';
      } else {
        url = `https://${apiUrl}/open/${prefix}/${id}/collections`;
        apiType = 'getUApi';
      }

      return await this.dispatch(apiType, {
        url,
      }).then(response => {
        if (data.callback) {
          data.callback(response);
        } else {
          return response;
        }
      }).catch(err => console.log(err));
    },
    async apiCreateCollection(context, data) {
      const { eventId, body, standId } = data;
      if (!eventId || !body) return false;
      const id = standId || eventId;
      const prefix = standId ? 'stand' : 'event';

      await this.dispatch('postApi', {
        url: `https://${apiUrl}/${prefix}/${id}/collection`,
        body: {
          name: body.name,
          ref: body.ref,
          tags: body.tags,
          event: eventId,
          stand: standId,
        },
      }).then( async (response) => {
        if (body.newStrings?.length) {
          const locale = context.rootState.i18n.locale;
          const strings = body.newStrings.map(string => {
            return {
              ...string,
              ref: 'collection',
              ref_id: response.data.body.id,
              language: locale
            };
          });

          await this.dispatch('postSingleString', strings);
        }

        if (body.image?.length) {
          await this.dispatch('uploadFiles', {
            id: standId || eventId,
            ref: 'collection',
            ref_id: response.data.body.id,
            post_type: standId ? 'stand' : 'event',
            files: body.image,
            category: 'branding',
            description: 'collection_thumb',
          });
        }

        if (body.hero?.length) {
          await this.dispatch('uploadFiles', {
            id: standId || eventId,
            ref: 'collection',
            ref_id: response.data.body.id,
            post_type: standId ? 'stand' : 'event',
            files: body.hero,
            category: 'branding',
            description: 'collection_hero',
          });
        }
      }).catch(err => console.log(`ERROR during collection create: ${err}`));
    },
    async apiUpdateCollectionById(context, data) {
      const { eventId, body, standId } = data;
      if (!eventId || !body || !body.id) return false;
      const id = standId || eventId;
      const prefix = standId ? 'stand' : 'event';

      return this.dispatch('putApi', {
        url: `https://${apiUrl}/${prefix}/${id}/collection/${body.id}`,
        body: {
          name: body.name,
          ref: body.ref,
          tags: body.tags,
          event: eventId,
          stand: standId,
        },
      }).then( async (response) => {
        if (body.newStrings?.length) {
          const locale = context.rootState.i18n.locale;
          const strings = body.newStrings.map(string => {
            return {
              ...string,
              ref: 'collection',
              ref_id: response.data.body.id,
              language: locale
            };
          });

          await this.dispatch('postSingleString', strings);
        }
        if (body.oldStrings?.length) {
          const locale = context.rootState.i18n.locale;
          const strings = body.oldStrings.map(string => {
            return {
              ...string,
              ref: 'collection',
              ref_id: response.data.body.id,
              language: locale
            };
          });

          await Promise.all(strings.map(string => {
            return this.dispatch('putSingleString', string);
          }));
        }

        if (body.image?.length) {
          const existedThumb = body.branding.find(item => item.url.indexOf('collection_thumb')>-1)
          if (existedThumb) {
            await this.dispatch('deleteFile', {
              id: existedThumb.id,
            });
          }

          await this.dispatch('uploadFiles', {
            id,
            ref: 'collection',
            ref_id: response.data.body.id,
            post_type: prefix,
            files: body.image,
            category: 'branding',
            description: 'collection_thumb',
          });
        }

        if (body.heroNew?.length) {
          const existedHero = body.branding.find(item => item.url.indexOf('collection_hero')>-1)
          if (existedHero) {
            await this.dispatch('deleteFile', {
              id: existedHero.id,
            });
          }

          await this.dispatch('uploadFiles', {
            id,
            ref: 'collection',
            ref_id: response.data.body.id,
            post_type: prefix,
            files: body.heroNew,
            category: 'branding',
            description: 'collection_hero',
          });
        }
      }).catch(err => console.log(`ERROR during collection update: ${err}`));
    },
    async getCollectionById(context, data) {
      if (!data.eventId || !data.collectionId) return false;
      const id = data.standId || data.eventId;
      const prefix = data.standId ? 'stand' : 'event';

      return this.dispatch('getApi', {
        url: `https://${apiUrl}/${prefix}/${id}/collection/${data.collectionId}`,
      }).catch(err => console.log(`ERROR during getting collection by id: ${err}`));
    },
    async apiDeleteCollectionById(context, data) {
      if (!data.eventId || !data.collectionId) return false;
      const id = data.standId || data.eventId;
      const prefix = data.standId ? 'stand' : 'event';

      return this.dispatch('deleteApi', {
        url: `https://${apiUrl}/${prefix}/${id}/collection/${data.collectionId}`,
      }).catch(err => console.log(`ERROR during collection delete: ${err}`));
    },
    async sendCollections(context, data) {
      const { collections, eventId, standId } = data;
      if (!eventId || (!collections.delete?.length && !collections.save?.length)) return false;

      // delete collections if there are any
      if (collections.delete?.length) {
        await Promise.all(collections.delete.map(collectionId => {
          return this.dispatch('apiDeleteCollectionById', { eventId, standId, collectionId });
        }));
      }

      if (collections.save?.length) {
        const collectionsToSave = collections.save.filter(collection => collection.new);
        const collectionsToUpdate = collections.save.filter(collection => !collection.new);

        // create new collections
        if (collectionsToSave.length) {
          await Promise.all(collectionsToSave.map(collection => {
            return this.dispatch('apiCreateCollection', { eventId, standId, body: collection });
          }));
        }

        // update existing collections
        if (collectionsToUpdate.length) {
          await Promise.all(collectionsToUpdate.map(collection => {
            return this.dispatch('apiUpdateCollectionById', { eventId, standId, body: collection });
          }));
        }
      }
    },
  },

  mutations: {

  },
  state: {

  },
  getters: {

  }
}
